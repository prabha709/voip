import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter/cupertino.dart';

class CallProvider extends ChangeNotifier {
  RtcEngine engine;
  initRtcEngine(RtcEngine eng) {
    engine = eng;
    notifyListeners();
  }
}
