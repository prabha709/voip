import 'package:app/components/messagebubble.dart';
import 'package:app/providers/auth.dart';
import 'package:app/providers/message.dart' as MProvider;
import 'package:app/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:app/providers/auth.dart' as AProvider;
import 'package:http/http.dart' as http;

class MessagesComponent extends StatefulWidget {
  final String id;
  final bool isGroup;
  MessagesComponent({
    @required this.id,
    @required this.isGroup,
  });

  @override
  _MessagesComponentState createState() => _MessagesComponentState();
}

class _MessagesComponentState extends State<MessagesComponent> {
  AProvider.AuthProvider authProvider;

  final _controller = ScrollController();
  @override
  void initState() {
    super.initState();
    authProvider = Provider.of<AProvider.AuthProvider>(context, listen: false);
    String token = Provider.of<AuthProvider>(context, listen: false).token;
    // Future.delayed(Duration.zero, () {
    //   Provider.of<MProvider.MessageProvider>(context, listen: false).getMessage(
    //     token,
    //     widget.id,
    //     widget.isGroup,
    //   );
    // });
  }

  // List _messages = [];
  // getMessageDetails() async* {
  //   String token = Provider.of<AuthProvider>(context, listen: false).token;
  //   _messages = [];
  //   print(token);
  //   print(widget.id);
  //   print(widget.isGroup);
  //   http.Response response = await http.get(
  //     "$apiurl/message/@${widget.id}?isgroup=${widget.isGroup}",
  //     headers: {
  //       "Content-type": "application/json",
  //       "Authorization": "Bearer $token"
  //     },
  //   );

  //   var decoded = json.decode(response.body);
  //   print(decoded);
  //   // decoded['data'].forEach((message) {
  //   //   _messages.add({
  //   //     'message': message['message'],
  //   //     'userid': message['userid'],
  //   //     'touserid': message['touserid'],
  //   //   });
  //   // });

  //   yield decoded["data"];
  // }

  @override
  Widget build(BuildContext context) {
    MProvider.MessageProvider messageProvider =
        Provider.of<MProvider.MessageProvider>(context, listen: false);
    String token = Provider.of<AuthProvider>(context, listen: false).token;
    return Container(
      child: messageProvider.loaderStatus == MProvider.LoaderStatus.busy
          ? Center(
              child: CupertinoActivityIndicator(),
            )
          : SingleChildScrollView(
              reverse: true,
              key: UniqueKey(),
              child: StreamBuilder(
                stream: messageProvider.getMessage(
                    token, widget.id, widget.isGroup),
                builder: (context, snapshot) {
                  if (snapshot.hasData == true) {
                    return ListView.builder(
                      shrinkWrap: true,
                      itemCount: messageProvider.messages.length,
                      itemBuilder: (context, index) {
                        return Container(
                          padding: EdgeInsets.all(8),
                          alignment: Alignment.centerRight,
                          child: Row(
                            mainAxisAlignment: authProvider.user.id ==
                                    messageProvider.messages[index]['userid']
                                        ['_id']
                                ? MainAxisAlignment.end
                                : MainAxisAlignment.start,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: authProvider.user.id ==
                                          snapshot.data[index]['userid']['_id']
                                      ? Colors.blueAccent[200]
                                      : Colors.blueAccent[100],
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(8),
                                    bottomRight: Radius.circular(8),
                                    topRight: authProvider.user.id !=
                                            snapshot.data[index]['userid']
                                                ['_id']
                                        ? Radius.circular(8)
                                        : Radius.zero,
                                    topLeft: authProvider.user.id ==
                                            snapshot.data[index]['userid']
                                                ['_id']
                                        ? Radius.circular(8)
                                        : Radius.zero,
                                  ),
                                ),
                                padding: EdgeInsets.all(8),
                                child: Column(
                                  crossAxisAlignment: authProvider.user.id !=
                                          snapshot.data[index]['userid']['_id']
                                      ? CrossAxisAlignment.start
                                      : CrossAxisAlignment.end,
                                  children: [
                                    Text(
                                      snapshot.data[index]['userid']['username']
                                          .toUpperCase(),
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 10,
                                      ),
                                    ),
                                    Text(
                                      snapshot.data[index]['message'],
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 18,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        );
                      },
                    );
                  } else {
                    return Text("No data found");
                  }
                },
              ),
              // child: Column(
              //   children: messageProvider.messages
              //       .map((m) => MessageBubble(data: m))
              //       .toList(),
              // ),
            ),
    );
  }
}
