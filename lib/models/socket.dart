import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:app/providers/call.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:socket_io_client/socket_io_client.dart';

class SocketModel {
  static final SocketModel _singleton = new SocketModel._internal();
  final String APP_ID = '7ccc18980cc34fe8b55adad2dc1dfc41';

  factory SocketModel() {
    return _singleton;
  }

  SocketModel._internal() {
    // Initialize values later, if any.
    if (_socket == null) {
      _socket = io('http://13.233.150.2:3000/', <String, dynamic>{
        'transports': ['websocket'],
        'autoConnect': false,
      });
    }
  }

  Socket _socket;
  Socket get socket => _socket;

  // Handle socket events
  enableListeners(BuildContext context) {
    socket.on('joined', (event) async {
      print('joined socketio server');
    });
    socket.on('video_call_offer', (data) async {
      print(data);
      print("video_call_offer triggerd");
      // Methods.showSnackBar('text', context);
      Navigator.pushNamed(context, 'attendcallscreen', arguments: data);
      // data
      //   {
      //     channelId:'',
      //     token:''
      //   }
      // await engine.joinChannel(decoded['token'], decoded['channelName'], null, 0);
      // Navigaotr.pop()
    });
    socket.on('audio_call_offer', (data) {
      print("audio_call_offer triggerd");
    });
    socket.on('call_end', (data) {
      print("call_end triggerd");
    });
  }
}
