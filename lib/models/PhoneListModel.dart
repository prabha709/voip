import 'dart:convert';

List<Welcome> welcomeFromJson(String str) =>
    List<Welcome>.from(json.decode(str).map((x) => Welcome.fromJson(x)));

String welcomeToJson(List<Welcome> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Welcome {
  Welcome({
    this.user,
    this.tahduthuser,
  });

  User user;
  bool tahduthuser;

  factory Welcome.fromJson(Map<String, dynamic> json) => Welcome(
        user: json["user"] == null ? null : User.fromJson(json["user"]),
        tahduthuser: json["tahduthuser"],
      );

  Map<String, dynamic> toJson() => {
        "user": user == null ? null : user.toJson(),
        "tahduthuser": tahduthuser,
      };
}

class User {
  User({
    this.displayimg,
    this.id,
    this.phone,
    this.v,
    this.channels,
    this.username,
  });

  String displayimg;
  String id;
  int phone;
  int v;
  List<Channel> channels;
  String username;

  factory User.fromJson(Map<String, dynamic> json) => User(
        displayimg: json["displayimg"],
        id: json["_id"],
        phone: json["phone"],
        v: json["__v"],
        channels: List<Channel>.from(
            json["channels"].map((x) => Channel.fromJson(x))),
        username: json["username"],
      );

  Map<String, dynamic> toJson() => {
        "displayimg": displayimg,
        "_id": id,
        "phone": phone,
        "__v": v,
        "channels": List<dynamic>.from(channels.map((x) => x.toJson())),
        "username": username,
      };
}

class Channel {
  Channel({
    this.id,
    this.private,
    this.group,
  });

  String id;
  String private;
  String group;

  factory Channel.fromJson(Map<String, dynamic> json) => Channel(
        id: json["_id"],
        private: json["private"],
        group: json["group"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "private": private,
        "group": group,
      };
}
