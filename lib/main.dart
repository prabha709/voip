import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:app/models/socket.dart';
import 'package:app/providers/auth.dart';
import 'package:app/providers/call.dart';
import 'package:app/providers/chat.dart';
import 'package:app/providers/message.dart';
import 'package:app/providers/securestorage.dart';
import 'package:app/router.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:app/providers/auth.dart' as AProvider;

import 'package:socket_io_client/socket_io_client.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initialization,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return MultiProvider(
            providers: [
              ChangeNotifierProvider(
                create: (context) => AuthProvider(),
              ),
              ChangeNotifierProvider(
                create: (context) => ChatProvider(),
              ),
              ChangeNotifierProvider(
                create: (context) => CallProvider(),
              ),
              ChangeNotifierProvider(
                create: (context) => MessageProvider(),
              ),
            ],
            child: MaterialApp(
              title: 'Flutter Demo',
              theme: ThemeData(
                visualDensity: VisualDensity.adaptivePlatformDensity,
              ),
              initialRoute: 'login',
              onGenerateRoute: RouteGenerator.generateRoute,
            ),
          );
        }
        return CupertinoActivityIndicator();
      },
    );
  }
}
