import 'package:app/providers/call.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AttendCallScreen extends StatelessWidget {
  final data;
  AttendCallScreen({
    @required this.data,
  });
  @override
  Widget build(BuildContext context) {
    CallProvider callProvider =
        Provider.of<CallProvider>(context, listen: false);
    return Scaffold(
      // floatingActionButton: FloatingActionButton(
      //   child: Text('ok'),
      //   onPressed: () {
      //     // callProvider.engine.enableVideo();
      //     // callProvider.engine
      //     //     .joinChannel(data["token"], data["userId"], null, 0);
      //     callProvider.engine.leaveChannel();
      //   },
      // ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FlatButton(
              color: Colors.green,
              onPressed: () {
                Navigator.pushReplacementNamed(
                  context,
                  'attendedcallscreen',
                  arguments: data,
                );
              },
              child: Text(
                'Answer',
                style: TextStyle(color: Colors.white),
              ),
            ),
            SizedBox(width: 8),
            FlatButton(
              color: Colors.red,
              onPressed: () {},
              child: Text(
                'Decline',
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}
