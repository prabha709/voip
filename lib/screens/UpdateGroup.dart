import 'package:app/providers/securestorage.dart';
import 'package:app/utils/app_colors.dart';
import 'package:app/utils/raised_button.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:multi_select_item/multi_select_item.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';

class UpdateGroup extends StatefulWidget {
  final groupId;

  UpdateGroup({this.groupId});

  @override
  _UpdateGroupState createState() => _UpdateGroupState();
}

class _UpdateGroupState extends State<UpdateGroup> {
  List contacts = [];
  MultiSelectController controller = new MultiSelectController();
  bool _isLoading = false;
  bool isSelected = false;
  List candiates = [];

  getContacts() async {
    if (contacts.length == null || contacts.length == 0) {
      Fluttertoast.showToast(
          msg: "NO Contacts available Please refresh your Contact list",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 5,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      final _storage = new FlutterSecureStorage();

      String token = await _storage.read(key: 'listContacts');
      final datas = json.decode(token);
      datas.forEach((element) {
        if (element['tahduthuser'] == true) {
          setState(() {
            _isLoading = false;
          });

          contacts.add(element);
          print(contacts.length);
        } else {
          print("fails");
        }
      });
    }
  }

  void selectAll() {
    setState(() {
      controller.toggleAll();
    });
  }

  @override
  void initState() {
    getContacts();
    controller.disableEditingWhenNoneSelected = true;
    controller.set(contacts.length);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SecureStorage _secureStorage = SecureStorage();
    updateMembers(context) async {
      String token = await _secureStorage.getToken();
      print(token);
      String url =
          "https://voipdatavivnew.herokuapp.com/group/${widget.groupId}";
      print(url);
      if (candiates.length == 0) {
        Fluttertoast.showToast(
            msg: "Please add Contacts to group",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.black,
            textColor: Colors.white,
            fontSize: 16.0);
      } else {
        Map data = {"participants": candiates};
        var response = await http.patch("$url",
            headers: {
              "Authorization": "Bearer $token",
              "Content-Type": "application/json"
            },
            body: json.encode(data));
        var jsonObject = json.decode(response.body);

        print(jsonObject);
      }
    }

    return WillPopScope(
      onWillPop: () async {
        //block app from quitting when selecting
        var before = !controller.isSelecting;
        setState(() {
          controller.deselectAll();
        });
        return before;
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.colorBlue,
            title: Text("Contacts"),
          ),
          body: contacts.length != 0
              ? ListView.builder(
                  shrinkWrap: true,
                  itemCount: contacts.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {},
                      child: MultiSelectItem(
                        isSelecting: controller.isSelecting,
                        onSelected: () {
                          controller.isSelected(index) == false
                              ? candiates.add(contacts[index]["user"]["_id"])
                              : candiates
                                  .remove(contacts[index]["user"]["_id"]);
                          print(candiates);
                          setState(() {
                            controller.toggle(index);
                          });
                        },
                        child: Card(
                          child: ListTile(
                            leading: CircleAvatar(),
                            title: Text(contacts[index]["user"]["username"]),
                            subtitle: Text(
                                contacts[index]["user"]["phone"].toString()),
                            trailing: controller.isSelected(index)
                                ? Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: Icon(
                                      Icons.check_circle,
                                      color: Colors.blue,
                                    ),
                                  )
                                : Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: Icon(
                                      Icons.check_circle_outline,
                                      color: Colors.blue,
                                    ),
                                  ),
                          ),
                        ),
                      ),
                    );
                  })
              : Container(
                  child: Center(
                    child: Text("No contact available"),
                  ),
                ),
          bottomNavigationBar: contacts.length != 0
              ? MyRaisedButton(
                  onPressed: updateMembers,
                  title: 'Add Members',
                  textColor: Colors.white,
                  loading: _isLoading,
                  buttonColor: AppColors.colorBlue,
                )
              : Container()),
    );
  }
}
