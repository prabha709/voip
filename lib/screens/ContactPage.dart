import 'package:app/providers/securestorage.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:app/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ContactsPage extends StatefulWidget {
  @override
  _ContactsPageState createState() => _ContactsPageState();
}

class _ContactsPageState extends State<ContactsPage> {
  Iterable<Contact> _contacts;
  List tahduthUser = [];
  bool loading = false;

  @override
  void initState() {
    uploadContact();
    super.initState();
  }

  SecureStorage _secureStorage = SecureStorage();
  // uploadContact() async{

  // }
  uploadContact() async {
    setState(() {
      loading = true;
    });
    List phoneList = [];
    final Iterable<Contact> contacts = await ContactsService.getContacts();
    contacts.forEach((element) {
      var phone = element.phones.elementAt(0).value;
      // String data1 = phone.replaceAll("+91", "");
      // String data2 = data1.replaceAll(" ", "");
      String conv = phone.replaceAll("-", "");
      String data = conv.replaceAll("(", "");
      String data1 = data.replaceAll(") ", "");
      String data2 = data1.replaceAll("[]", "");

      phoneList.add(data2);
    });
    String token = await _secureStorage.getToken();
    print(token);

    String url = "http://65.1.0.127:3000/checkusers";
    Map data = {"phonelist": phoneList};

    var response = await http.post("$url",
        headers: {
          "Authorization": "Bearer $token",
          "Content-Type": "application/json"
        },
        body: json.encode(data));
    var jsonObject = json.decode(response.body);

    jsonObject.forEach((element) {
      if (element['tahduthuser'] == true) {
        setState(() {
          loading = false;
        });
        tahduthUser.add(element);
      } else {
        Fluttertoast.showToast(
            msg: "No One Using Our App from your contacts",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.black,
            fontSize: 16.0);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.colorBlue,
        title: (Text('Contacts')),
      ),
      body: loading == false
          ? ListView.builder(
              itemCount: tahduthUser.length,
              itemBuilder: (context, index) {
                return Card(
                  child: ListTile(
                    leading: CircleAvatar(),
                    title: Text(tahduthUser[index]["user"]["username"]),
                    subtitle:
                        Text(tahduthUser[index]["user"]["phone"].toString()),
                  ),
                );
              })
          : Center(
              child: CircularProgressIndicator(
                backgroundColor: AppColors.colorBlue,
              ),
            ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),
        onPressed: () {
          uploadContact();
        },
      ),
    );
  }
}
