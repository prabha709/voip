import 'package:app/providers/auth.dart';
import 'package:app/providers/securestorage.dart';
import 'package:app/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../utils/app_colors.dart';
import '../utils/raised_button.dart';
import 'package:file_picker/file_picker.dart';
import 'dart:io';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Profile extends StatefulWidget {
  final phoneNumber;

  Profile({this.phoneNumber});

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  bool loading = false;
  File _image;
  String _phone;
  String _userName;
  _pickImage() async {
    var image = await FilePicker.getFile(
      type: FileType.image,
    );

    if (image != null) {
      setState(() {
        _image = image;
      });
    } else {
      setState(() {
        _image = null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SecureStorage _secureStorage = SecureStorage();
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    _update(context) async {
      String url = "https://voipdatavivnew.herokuapp.com/user";
      if (_formkey.currentState.validate()) {
        setState(() {
          loading = true;
        });

        _formkey.currentState.save();
        if (_image == null) {
          Fluttertoast.showToast(
              msg: "Kindly Select an Image",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.black,
              textColor: Colors.white,
              fontSize: 16.0);
          setState(() {
            loading = false;
          });
        }
        String token = await _secureStorage.getToken();
        print(token);
        var request = http.MultipartRequest("POST", Uri.parse("$url"));
        request.headers["Authorization"] = "Bearer $token}";
        request.files.add(await http.MultipartFile.fromPath(
          "displayimg",
          _image.path,
        ));
        request.fields["username"] = _userName;

        request.send().then((result) async {
          http.Response.fromStream(result).then((response) {
            if (response.statusCode == 200) {
              print(response.statusCode);
              Navigator.pop(context);
              Fluttertoast.showToast(
                  msg: "Profile Updated",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 5,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: 16.0);
              //  Methods.showSnackBar("Employee Created Successfully", context);
            } else {
              print(response.statusCode);
              setState(() {
                loading = false;
              });
              Fluttertoast.showToast(
                  msg: "${response.body}".toString(),
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 5,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: 16.0);
              print("${response.body}");
              // Methods.showSnackBar("Something Went Wrong Please try again later", context);
            }
          });
        });
        //   Map data = {"displayimg": _image.toString, "username": _userName};
        //   var response = await http.post("$url",
        //       headers: {"Authorization": "Bearer $token"}, body: data);
        //   var jsonObject = json.decode(response.body);
        //   if (response.statusCode == 200) {
        //     Navigator.pop(context);
        //     Fluttertoast.showToast(
        //         msg: "Profile Udpated",
        //         toastLength: Toast.LENGTH_SHORT,
        //         gravity: ToastGravity.BOTTOM,
        //         timeInSecForIosWeb: 1,
        //         backgroundColor: Colors.black,
        //         textColor: Colors.white,
        //         fontSize: 16.0);
        //   } else {
        //     print(jsonObject);
        //     setState(() {
        //       loading = false;
        //     });
        //     Fluttertoast.showToast(
        //         msg: "$jsonObject".toString(),
        //         toastLength: Toast.LENGTH_SHORT,
        //         gravity: ToastGravity.BOTTOM,
        //         timeInSecForIosWeb: 5,
        //         backgroundColor: Colors.black,
        //         textColor: Colors.white,
        //         fontSize: 16.0);
        //   }
        // }
      }
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // automaticallyImplyLeading: false,
        elevation: 0.0,
        backgroundColor: AppColors.colorBlue,
        centerTitle: true,
        title: Text(
          "Profile",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      body: Form(
        key: _formkey,
        child: SingleChildScrollView(
            child: Column(
          children: [
            Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 28.0),
                  child: Align(
                      alignment: Alignment.topCenter,
                      child: CircleAvatar(
                        radius: 80.0,
                        backgroundColor: Colors.grey[300],
                        backgroundImage: _image == null
                            ? AssetImage("assets/contact.ico")
                            : FileImage(_image),
                      )),
                ),
                Transform.translate(
                  offset: Offset(250, 120),
                  child: InkWell(
                    onTap: () {
                      _pickImage();
                    },
                    child: CircleAvatar(
                      radius: 25.0,
                      backgroundColor: AppColors.colorBlue,
                      child: Center(
                          child: Icon(
                        Icons.camera_alt,
                        color: Colors.white,
                        size: 30.0,
                      )),
                    ),
                  ),
                )
              ],
            ),
            Padding(
              padding:
                  const EdgeInsets.only(top: 18.0, left: 24.0, right: 24.0),
              child: ListTile(
                leading: Icon(Icons.account_circle_outlined),
                title: TextFormField(
                  onSaved: (value) => _userName = value,
                  initialValue: authProvider.user.username,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please Enter UserName';
                    } else {
                      return null;
                    }
                  },
                  decoration: InputDecoration(
                    labelText: "UserName",
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context).secondaryHeaderColor),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context).secondaryHeaderColor),
                    ),
                    border: UnderlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context).secondaryHeaderColor),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(top: 18.0, left: 24.0, right: 24.0),
              child: ListTile(
                leading: Icon(Icons.phone),
                title: TextFormField(
                  enabled: false,
                  initialValue: authProvider.user.phone.toString(),
                  // validator: (value) {
                  //   if (value.length < 10) {
                  //     return 'Please Enter UserName';
                  //   } else {
                  //     return null;
                  //   }
                  // },
                  // onSaved: (value) => _phone = value,
                  decoration: InputDecoration(
                    labelText: "Phone",
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context).secondaryHeaderColor),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context).secondaryHeaderColor),
                    ),
                    border: UnderlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context).secondaryHeaderColor),
                    ),
                  ),
                ),
              ),
            ),
            // Padding(
            //   padding:
            //       const EdgeInsets.only(top: 18.0, left: 24.0, right: 24.0),
            //   child: TextFormField(
            //     validator: Validators.compose([
            //       Validators.required('Password is required'),
            //       Validators.patternString(
            //           r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$',
            //           'Invalid Password\n Example Password: Test@123')
            //     ]),
            //     decoration: InputDecoration(
            //       labelText: "Password",
            //       enabledBorder: UnderlineInputBorder(
            //         borderSide: BorderSide(
            //             color: Theme.of(context).secondaryHeaderColor),
            //       ),
            //       focusedBorder: UnderlineInputBorder(
            //         borderSide: BorderSide(
            //             color: Theme.of(context).secondaryHeaderColor),
            //       ),
            //       border: UnderlineInputBorder(
            //         borderSide: BorderSide(
            //             color: Theme.of(context).secondaryHeaderColor),
            //       ),
            //     ),
            //   ),
            // ),
            Padding(
              padding: const EdgeInsets.only(top: 28.0),
              child: MyRaisedButton(
                loading: loading,
                onPressed: _update,
                title: "Update",
                buttonColor: AppColors.colorBlue,
                textColor: AppColors.colorWhite,
              ),
            ),
          ],
        )),
      ),
    );
  }
}
