import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:app/components/search.dart';
import 'package:app/models/group.dart';
import 'package:app/models/socket.dart';
import 'package:app/providers/auth.dart' as AProvider;
import 'package:app/providers/call.dart';
import 'package:app/providers/chat.dart' as CProvider;
import 'package:app/providers/message.dart';
import 'package:app/providers/securestorage.dart';
import 'package:app/screens/ContactPage.dart';
import 'package:app/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'package:web_socket_channel/io.dart';
import '../utils/app_colors.dart';
import 'package:permission_handler/permission_handler.dart';

class ChatList extends StatefulWidget {
  @override
  _ChatListState createState() => _ChatListState();
}

class _ChatListState extends State<ChatList> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Widget appBarTitle = new Text("Tahduth");
  Icon actionIcon = new Icon(Icons.search);
  IOWebSocketChannel channel;
  final String appId = '92ab36aa9cb04bbfb10f57eddc95aa2e';

  @override
  void initState() {
    super.initState();
    AProvider.AuthProvider authProvider =
        Provider.of<AProvider.AuthProvider>(context, listen: false);
    channel = IOWebSocketChannel.connect(
      'wss://api.tahduth-app.com/${authProvider.token}',
    );
    MessageProvider messageProvider =
        Provider.of<MessageProvider>(context, listen: false);
    CProvider.ChatProvider chatProvider =
        Provider.of<CProvider.ChatProvider>(context, listen: false);
    channel.stream.listen((event) {
      messageProvider.handle(event);
      chatProvider.handle(event);
    });
    connectToServer();
    createEngine();
    Provider.of<CProvider.ChatProvider>(context, listen: false).getChat();
  }

  void createEngine() async {
    RtcEngine engine = await RtcEngine.create(appId);
    CallProvider callProvider =
        Provider.of<CallProvider>(context, listen: false);
    callProvider.initRtcEngine(engine);
  }

  void connectToServer() async {
    FlutterSecureStorage secureStorage = SecureStorage.instance;
    String userid = await secureStorage.read(key: 'id');
    try {
      // Configure socket transports must be sepecified
      SocketModel socketModel = SocketModel();
      Socket socket = socketModel.socket;
      // Connect to websocket
      socket.connect();

      socket.emit("join", userid);

      socketModel.enableListeners(context);
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    CProvider.ChatProvider chatProvider =
        Provider.of<CProvider.ChatProvider>(context);
    MessageProvider messageProvider = Provider.of<MessageProvider>(context);
    AProvider.AuthProvider authProvider =
        Provider.of<AProvider.AuthProvider>(context);

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: appBarTitle,
        actions: [
          IconButton(
            icon: actionIcon,
            onPressed: () async {
              setState(() {
                if (this.actionIcon.icon == Icons.search) {
                  this.actionIcon = new Icon(Icons.close);
                  this.appBarTitle = SearchComponent();
                } else {
                  this.actionIcon = new Icon(Icons.search);
                  this.appBarTitle = new Text("Tahduth");
                  chatProvider.getChat();
                }
              });
            },
          )
        ],
        backgroundColor: AppColors.colorBlue,
      ),

      key: _scaffoldKey,
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     print('ok');
      //     messageProvider.handle({"message": "This is a message"});
      //   },
      //   tooltip: 'Send message',
      //   child: Icon(Icons.send),
      // ),
      body: Container(
        child: chatProvider.loaderStatus == CProvider.LoaderStatus.busy
            ? Center(
                child: CupertinoActivityIndicator(),
              )
            : chatProvider.users.length != 0
                ? ListView.builder(
                    // reverse: true,
                    itemCount: chatProvider.users.length,
                    itemBuilder: (_, i) {
                      return ListTile(
                        contentPadding: EdgeInsets.all(8),
                        onTap: () {
                          // print("userid${chatProvider.users[i].id}");
                          Navigator.pushNamed(
                            context,
                            'message',
                            arguments: {
                              "username":
                                  chatProvider.users[i].runtimeType != Group
                                      ? chatProvider.users[i].username
                                      : chatProvider.users[i].title,
                              "id": chatProvider.users[i].id,
                              "displayimg": chatProvider.users[i].displayimg,
                              "channel": channel,
                              "isGroup":
                                  chatProvider.users[i].runtimeType == Group
                                      ? true
                                      : false,
                            },
                          ).then((value) async {
                            await chatProvider.getChat();
                          });
                        },
                        // tileColor: Colors.grey[100],
                        leading: CircleAvatar(
                          backgroundImage: NetworkImage(
                            apiurl + chatProvider.users[i].displayimg,
                          ),
                          radius: 32,
                        ),
                        title: Text(
                          chatProvider.users[i].runtimeType != Group
                              ? chatProvider.users[i].username
                              : chatProvider.users[i].title,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        // subtitle: Text('last message'),
                      );
                    },
                  )
                : Center(
                    child: Text('You haven\'t texted anyone.'),
                  ),
      ),
      // floatingActionButton: FloatingActionButton(
      //     backgroundColor: AppColors.colorBlue,
      //     child: Icon(
      //       Icons.chat,
      //       color: AppColors.colorWhite,
      //     ),
      //     onPressed: () async {
      //       final PermissionStatus permissionStatus = await _getPermission();
      //       if (permissionStatus == PermissionStatus.granted) {
      //         Navigator.push(context,
      //             MaterialPageRoute(builder: (context) => ContactsPage()));
      //         //We can now access our contacts here
      //       } else {
      //         showDialog(
      //             context: context,
      //             builder: (BuildContext context) => CupertinoAlertDialog(
      //                   title: Text('Permissions error'),
      //                   content: Text('Please enable contacts access '
      //                       'permission in system settings'),
      //                   actions: <Widget>[
      //                     CupertinoDialogAction(
      //                       child: Text('OK'),
      //                       onPressed: () => Navigator.of(context).pop(),
      //                     )
      //                   ],
      //                 ));
      //       }
      //     }),
    );
  }

  Future<PermissionStatus> _getPermission() async {
    final PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.denied) {
      final Map<Permission, PermissionStatus> permissionStatus =
          await [Permission.contacts].request();
      return permissionStatus[Permission.contacts] ??
          PermissionStatus.undetermined;
    } else {
      return permission;
    }
  }
}
