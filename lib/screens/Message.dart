import 'package:app/components/message.dart';
import 'package:app/components/send.dart';
import 'package:app/screens/UpdateGroup.dart';
import 'package:flutter/material.dart';
import '../utils/app_colors.dart';

List<String> messageList = [];
String tokenData;

class Message extends StatefulWidget {
  final username;
  final displayimg;
  final id;
  final channel;
  final bool isGroup;
  Message({
    @required this.username,
    @required this.displayimg,
    @required this.id,
    @required this.channel,
    @required this.isGroup,
  });

  @override
  _MessageState createState() => _MessageState();
}

class _MessageState extends State<Message> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.chevron_left,
              size: 35.0,
            )),
        title: Row(
          children: [
            CircleAvatar(
              radius: 22,
            ),
            SizedBox(
              width: 8,
            ),
            Text(widget.username),
          ],
        ),
        actions: [
          widget.isGroup == true
              ? Padding(
                  padding: const EdgeInsets.only(right: 18.0),
                  child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => UpdateGroup(
                                      groupId: widget.id,
                                    )));
                      },
                      child: Icon(Icons.group_add)),
                )
              : IconButton(
                  onPressed: () {
                    print('clicked');
                    Navigator.pushNamed(
                      context,
                      'callscreen',
                      arguments: widget.id,
                    );
                  },
                  icon: Icon(
                    Icons.call,
                  ),
                )
        ],
        backgroundColor: AppColors.colorBlue,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.fill, image: AssetImage("assets/texture13.png"))),
        child: Column(
          children: [
            Expanded(
              child: MessagesComponent(
                id: widget.id,
                isGroup: widget.isGroup,
              ),
            ),
            SendComponent(
              touserid: widget.id,
              channel: widget.channel,
              isGroup: widget.isGroup,
            ),
          ],
        ),
      ),
    );
  }
}
