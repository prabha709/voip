import 'package:app/screens/AttendCall.dart';
import 'package:app/screens/AttendedCall.dart';
import 'package:app/screens/CallKeep.dart';
import 'package:app/screens/CallScreen.dart';
import 'package:app/screens/ChatList.dart';
import 'package:app/screens/CreateGroup.dart';
import 'package:app/screens/LoginPage.dart';
import 'package:app/screens/Message.dart';
import 'package:app/screens/SignUp.dart';
import 'package:app/screens/test.dart';
import 'package:flutter/material.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    print(settings);
    switch (settings.name) {
      case 'login':
        return MaterialPageRoute(builder: (_) => LoginPage());
      case 'home':
        return MaterialPageRoute(builder: (_) => ChatList());
      case 'test':
        return MaterialPageRoute(builder: (_) => MyApp());
      case 'creategroup':
        return MaterialPageRoute(builder: (_) => CreateGroup());
      case 'attendcallscreen':
        return MaterialPageRoute(
          builder: (_) => AttendCallScreen(data: settings.arguments),
        );
      case 'attendedcallscreen':
        return MaterialPageRoute(
          builder: (_) => AttendedCallScreen(data: settings.arguments),
        );

      case 'callscreen':
        return MaterialPageRoute(
          builder: (_) => CallScreen(channelId: settings.arguments as String),
        );
      case 'message':
        Map data = settings.arguments as Map;

        return MaterialPageRoute(
          builder: (_) => Message(
            username: data['username'],
            id: data['id'],
            displayimg: data['displayimg'],
            channel: data['channel'],
            isGroup: data['isGroup'],
          ),
        );
      case 'signup':
        String phoneno = settings.arguments as String;
        print(phoneno);
        return MaterialPageRoute(
          builder: (_) => SignUp(
            phone: "$phoneno".toString(),
          ),
        );
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Container(
              child: Center(
                child: Text('Check Route Name'),
              ),
            ),
          ),
        );
    }
  }
}
